{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Space- and time-dependent external source\n",
    "\n",
    "### Authors: Esra Ilke Albar, Franco Bonafé, Carlos Bustamante, Heiko Appel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Within the new multisystem framework, we can use the `ExternalSource` feature for simulations with space dependent laser pulses. It gives us the possibility to assign a laser that is both a function of space and time. The purpose of this tutorial is to learn how to convert the time-dependent external fields input variables, defined in the \"legacy-mode\" of Octopus via `TDExternalFields`, to space- and time-dependent fields using `ExternalSource`.\n",
    "\n",
    "The reference calculation using `ExternalSource` is the one covered in the [full minimal coupling tutorial](./tutorial_full_minimal.ipynb). In that input file, we have set an external field that can be written as:\n",
    "\n",
    "$ A(\\vec{r}, t) = A_0 \\exp (i(\\vec{k}\\cdot\\vec{r} - \\omega t + \\phi)) \\exp(\\frac{−(\\vec{k}\\cdot (\\vec{r} - \\vec{r}_0)/|\\vec{k}|)^2}{2 \\sigma^2})$\n",
    "\n",
    "where $k=\\omega/c$ using the following block in the input file:\n",
    "\n",
    "```\n",
    "AnalyticalExternalSource = yes\n",
    "%ExternalSource.MaxwellIncidentWaves\n",
    " plane_wave_mx_function | vector_potential | -ampl*c/omega | 0 | 0 | \"plane_waves_function\" | phase_vec\n",
    "%\n",
    "%MaxwellFunctions\n",
    "  \"plane_waves_function\" | mxf_gaussian_wave | 0 | omega/c | 0 | 0 | p_s | 0 | pw\n",
    "%\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will run the equivalent simulation with TDExternalFields to compare. We create new folders for the required ground state calculation and one for the time propagation called TD_EXT_FIELDS_DIR"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "from postopus import Run\n"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "Prepare a ground state calculation in time dependent folder to read from:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.path.exists('GS_DIR'):\n",
    "    os.mkdir('GS_DIR')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile GS_DIR/benzene.xyz\n",
    "12\n",
    "units: A\n",
    "      C                    0.000000    1.364239    0.000000\n",
    "      C                    1.191347    0.685530    0.000000\n",
    "      C                    1.191347   -0.685530    0.000000\n",
    "      C                    0.000000   -1.364239    0.000000\n",
    "      C                   -1.191347   -0.685530    0.000000\n",
    "      C                   -1.191347    0.685530    0.000000\n",
    "      H                    0.000000    2.441134    0.000000\n",
    "      H                    2.124739    1.238817    0.000000\n",
    "      H                    2.124739   -1.238817    0.000000\n",
    "      H                    0.000000   -2.441134    0.000000\n",
    "      H                   -2.124739   -1.238817    0.000000\n",
    "      H                   -2.124739    1.238817    0.000000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile GS_DIR/inp\n",
    "\n",
    "CalculationMode = gs\n",
    "ExperimentalFeatures = yes\n",
    "FromScratch = yes\n",
    "Dimensions = 3\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "UnitsXYZFiles = angstrom_units\n",
    "AllElectronType = full_delta\n",
    "\n",
    "ExtraStates = 1\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "Spacing = 0.1*angstrom\n",
    "%Lsize\n",
    " 3.5*angstrom | 3.5*angstrom | 1.5*angstrom\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cd GS_DIR\n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -np 6 octopus >& out.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and we run the following input files for time dependent calculation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.path.exists('TD_EXT_FIELDS_DIR'):\n",
    "    os.mkdir('TD_EXT_FIELDS_DIR')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile TD_EXT_FIELDS_DIR/benzene.xyz\n",
    "\n",
    "  12\n",
    " units: A\n",
    "      C                    0.000000    1.364239    0.000000\n",
    "      C                    1.191347    0.685530    0.000000\n",
    "      C                    1.191347   -0.685530    0.000000\n",
    "      C                    0.000000   -1.364239    0.000000\n",
    "      C                   -1.191347   -0.685530    0.000000\n",
    "      C                   -1.191347    0.685530    0.000000\n",
    "      H                    0.000000    2.441134    0.000000\n",
    "      H                    2.124739    1.238817    0.000000\n",
    "      H                    2.124739   -1.238817    0.000000\n",
    "      H                    0.000000   -2.441134    0.000000\n",
    "      H                   -2.124739   -1.238817    0.000000\n",
    "      H                   -2.124739    1.238817    0.000000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile TD_EXT_FIELDS_DIR/inp\n",
    "\n",
    "CalculationMode = td\n",
    "ExperimentalFeatures = yes\n",
    "FromScratch = yes\n",
    "RestartWallTimePeriod = 10.01\n",
    "ParStates = no\n",
    "\n",
    "%Systems\n",
    " \"benzene\" | electronic\n",
    "%\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "UnitsXYZFiles = angstrom_units\n",
    "AllElectronType = full_delta\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "Spacing = 0.1*angstrom\n",
    "%Lsize\n",
    " 3.5*angstrom | 3.5*angstrom | 1.5*angstrom\n",
    "%\n",
    "\n",
    "%TDOutput\n",
    " multipoles\n",
    "%\n",
    "\n",
    "omega = 270*ev\n",
    "period = 0.015*fs\n",
    "ampl = 0.0001\n",
    "tau0 = 1*period # sigma\n",
    "t0 = tau0*3\n",
    "phase_vec = -pi/2\n",
    "phase0 = 0.51\n",
    "\n",
    "TDSystemPropagator = prop_aetrs\n",
    "benzene.TDTimeStep = 0.008\n",
    "TDPropagationTime = 7*period\n",
    "\n",
    "%TDExternalFields\n",
    " vector_potential | -1 | 0 | 0 | omega | \"gauss\" | \"phase\"\n",
    "%\n",
    "\n",
    "%TDFunctions\n",
    " \"gauss\" | tdf_gaussian | ampl*c/omega | tau0 | t0\n",
    " \"phase\" | tdf_cw | phase_vec + phase0\n",
    "%"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running Octopus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp -a GS_DIR TD_EXT_FIELDS_DIR/benzene\n",
    "cd TD_EXT_FIELDS_DIR\n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -np 6 octopus > out.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As can be seen, we are considering the effect of an external laser defined with the usual time-dependent function:\n",
    "\n",
    "$ A(t) = A_0 \\exp (-i (\\omega t + \\phi)) \\exp(−\\frac{(t−t_0)^2)}{2 \\tau_0^2}) $\n",
    "\n",
    "which should be equal to the previous expression for $A(\\vec{r},t)$ for $\\vec{r} = 0$ (or $\\vec{r}$ equal to the center of mass/center of charge of the system, depending on the definition of \"center\" that is being used). Here, we do not need to set any level of light-matter coupling, as the level of coupling is dipole.\n",
    "\n",
    "Let us plot and look the multipole response of the molecule for two cases now. You can use [Postopus](https://gitlab.com/octopus-code/postopus) to probe the response to two lasers:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As also done in Full Minimal Coupling tutorials, we can perform the space dependent calculations and compare with the only time dependent laser."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.path.exists('EXT_SOURCE_DIPOLE_DIR'):\n",
    "    os.mkdir('EXT_SOURCE_DIPOLE_DIR')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile EXT_SOURCE_DIPOLE_DIR/benzene.xyz\n",
    "12\n",
    "units: A\n",
    "      C                    0.000000    1.364239    0.000000\n",
    "      C                    1.191347    0.685530    0.000000\n",
    "      C                    1.191347   -0.685530    0.000000\n",
    "      C                    0.000000   -1.364239    0.000000\n",
    "      C                   -1.191347   -0.685530    0.000000\n",
    "      C                   -1.191347    0.685530    0.000000\n",
    "      H                    0.000000    2.441134    0.000000\n",
    "      H                    2.124739    1.238817    0.000000\n",
    "      H                    2.124739   -1.238817    0.000000\n",
    "      H                    0.000000   -2.441134    0.000000\n",
    "      H                   -2.124739   -1.238817    0.000000\n",
    "      H                   -2.124739    1.238817    0.000000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile EXT_SOURCE_DIPOLE_DIR/inp\n",
    "\n",
    "CalculationMode = td\n",
    "ExperimentalFeatures = yes\n",
    "FromScratch = yes\n",
    "RestartWallTimePeriod = 10.01\n",
    "ParStates = no\n",
    "\n",
    "%Systems\n",
    " \"benzene\" | electronic\n",
    "%\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "UnitsXYZFiles = angstrom_units\n",
    "AllElectronType = full_delta\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "benzene.Spacing = 0.1*angstrom\n",
    "%benzene.Lsize\n",
    " 3.5*angstrom | 3.5*angstrom | 1.5*angstrom\n",
    "%\n",
    "\n",
    "%TDOutput\n",
    " maxwell_field\n",
    " multipoles\n",
    "%\n",
    "%Output\n",
    " density | plane_z\n",
    "%\n",
    "OutputInterval = 20\n",
    "\n",
    "omega = 270*ev\n",
    "period = 0.015*fs\n",
    "ampl = 0.0001\n",
    "tau0 = 1*period # sigma\n",
    "t0 = tau0*3\n",
    "p_s = - t0*c\n",
    "pw  = tau0*c\n",
    "phase_vec = -pi/2\n",
    "\n",
    "TDSystemPropagator = prop_aetrs\n",
    "benzene.TDTimeStep = 0.008\n",
    "TDPropagationTime = 7*period\n",
    "\n",
    "MaxwellCouplingMode = velocity_gauge_dipole\n",
    "AnalyticalExternalSource = yes\n",
    "%ExternalSource.MaxwellIncidentWaves\n",
    " plane_wave_mx_function | vector_potential | -ampl*c/omega | 0 | 0 | \"plane_waves_function\" | phase_vec\n",
    "%\n",
    "%MaxwellFunctions\n",
    "  \"plane_waves_function\" | mxf_gaussian_wave | 0 | omega/c | 0 | 0 | p_s | 0 | pw\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp -a GS_DIR EXT_SOURCE_DIPOLE_DIR/benzene\n",
    "cd EXT_SOURCE_DIPOLE_DIR\n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -np 6 octopus > out.log"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run_ext_source = Run(\"EXT_SOURCE_DIPOLE_DIR\")\n",
    "run_td_ext_fields = Run(\"TD_EXT_FIELDS_DIR\")\n",
    "\n",
    "ax = run_ext_source.benzene.td.multipoles.plot(x=\"t\", y= \"<x>(1)\", label =\"External source $A(r,t)$ - dipole-level\")\n",
    "run_td_ext_fields.benzene.td.multipoles.plot(x=\"t\", y= \"<x>(1)\", label = \"TD External Fields $A(t)$ - dipole-level\", ax=ax);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see from the plot, time dependent laser and space dependent laser can be engineered to give the same dipole response at the position of the molecule. However, space dependence can be crucial in cases where a beyond dipole response is expected, as we will see below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Exercise_:\n",
    "1. It is visible that the dipole signals are not exactly equal. This difference is already present in the external fields. Plot the external fields from both calculations, comparing the outputs in `EXT_SOURCE_DIPOLE_DIR/benzene/td.general/maxwell_dipole_field` and `TD_EXT_FIELDS_DIR/benzene/td.general/laser`. Keep in mind that the `maxwell_dipole_field` output is already rescaled by $1/c$ ($c= 137.03599$ is the speed of light in atomic units), so you have to scale it back by a factor of $c$. What could be the reason between the difference of both fields? What are the parameters that makes two fields similar/different? _Hint: think about how one can define a \"space-independent\" field, if all fields are space-dependent_.\n",
    "\n",
    "_Questions_:\n",
    "\n",
    "1. Which external field/conditions would yield large effects when the space dependence is included? \n",
    "2. Do you think quadrupole effects can be described with a time dependent source?\n",
    "3. Which calculation runs faster: the one using `TDExternalFields` or `ExternalSource`? What could be the reason for the different timings?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Help for the comparison of the external fields:\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "efield_extsource = run_ext_source.benzene.td.maxwell_dipole_field\n",
    "efield_legacy = run_td_ext_fields.benzene.td.laser\n",
    "\n",
    "plt.plot(efield_extsource.to_numpy()[:,1],efield_extsource.to_numpy()[:,2]*(137))\n",
    "plt.plot(efield_legacy.to_numpy()[:,0],efield_legacy.to_numpy()[:,1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
